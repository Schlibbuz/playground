const fs = require('fs');

const path = 'resources/requests.json';

exports.readJSON = (path, opts) => {
    if (path === undefined) path = 'resources/requests.json';
    if (opts === undefined) {
        opts = {};
        opts.encoding = 'utf8';
    }
    return fs.readFileSync(path, opts.encoding);
};

exports.initReqsPerMin = () => {
    const reqsPerMin = {};

    for (let hour = 0; hour < 24; hour++) {
        for (let minute = 0; minute < 60; minute++) {
            const key =
                hour.toString().padStart(2, '0') +
                ':' +
                minute.toString().padStart(2, '0');
            reqsPerMin[key] = 0;
        }
    }

    return reqsPerMin;
};

exports.genReqsPerMin = requests => {
    const reqsPerMin = this.initReqsPerMin();

    requests.forEach(obj => {
        const key = obj.datetime.hour + ':' + obj.datetime.minute;
        reqsPerMin[key]++;
    });
    return reqsPerMin;
};

exports.genDistHttpMeths = requests => {
    const reqsGET = requests.filter(req => req.request.method === 'GET').length;
    const reqsPOST = requests.filter(req => req.request.method === 'POST')
        .length;
    const reqsHEAD = requests.filter(req => req.request.method === 'HEAD')
        .length;

    return { GET: reqsGET, POST: reqsPOST, HEAD: reqsHEAD };
};

exports.genDistResCodes = requests => {
    const reqs200 = requests.filter(req => req.response_code === 200).length;
    const reqs301 = requests.filter(req => req.response_code === 301).length;
    const reqs304 = requests.filter(req => req.response_code === 304).length;

    return { '200': reqs200, '301': reqs301, '304': reqs304 };
};

exports.genDist200BodySize = (requests, statusCode = 200, maxSize = 1000) => {
    requests = requests.filter(req => {
        return req.response_code == 200 && req.document_size < 1000;
    });
};

exports.genStats = (type, requests) => {
    if (requests === undefined) requests = JSON.parse(this.readJSON(path));

    if (type === 'reqsPerMin') return this.genReqsPerMin(requests);

    if (type === 'distHttpMeths') return this.genDistHttpMeths(requests);

    if (type === 'distResCodes') return this.genDistResCodes(requests);

    if (type === 'dist200BodySize') return this.genDist200BodySize(requests);
};

/**
 *
 *
 */
const main = () => {
    this.genStats('distResCodes');
};

if (require.main === module) {
    main();
}
