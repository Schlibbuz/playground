const fs = require('fs');

const constants = require('./constants');

/**
 * reads file from path
 *
 * @param {string} path -
 *
 * @return {string[]} -
 */
exports.readLines = path => {
    return (
        fs
            .readFileSync('resources/epa-http.txt', 'utf8')
            //clean empty lines on start and end
            .trim()
            //try to be platform-independent
            .replace('\r\n', '\n')
            .split('\n')
    );
};

/**
 * writes a JSON-Format file to path
 *
 * @param {dict[]} reqArr -
 * @param {string} path -
 *
 * @return {number} -
 */
exports.writeJSON = (reqArr, path) => {
    if (path === undefined) path = 'resources/requests.json';

    fs.writeFile(path, JSON.stringify(reqArr, null, 4), function(err) {
        if (err) return console.log(err);
        console.log(`requests.json written to ${path}!`);
        return 0;
    });
};

/**
 * cuts off enclosing square-brackets
 *
 * @param {string} time -
 *
 * @return {string} string with 4 colon-separated time-values
 */
exports.getTimeString = time => {
    return time.trim().substring(1, time.length - 1);
};

/**
 * converts colon-separated time-string to array
 *
 * @param {string} timeString -
 *
 * @return {string[]} - [day, hour, minute, second]
 */
exports.getTimeParts = timeString => {
    return timeString.split(':');
};

/**
 * takes a time argument as either 'x:x:x:x' {string} or ['x', 'x', 'x', 'x'] {array}
 *
 * @param {string[] or string} time -
 *
 * @return {dict} - dict(day: x, hour: x, minute: x, second: x)
 */
exports.getTimeObj = time => {
    if (typeof time == 'string') time = this.getTimeParts(time);
    //if (time.length != 4) throw ParseError("invalid arr-length, must be 4");
    return {
        day: time[0],
        hour: time[1],
        minute: time[2],
        second: time[3],
    };
};

/**
 * split protocol-string into protocol and protocol-version
 *
 * @param {string} protoString - protocol-string e.g HTTP/1.0
 *
 * @return {string}
 */
exports.getProtoParts = protoString => {
    return protoString.split('/');
};

/**
 * tokenizer for logfile-lines.
 *
 * @param {string} line - example here: 141.243.1.172 [29:23:53:25] "GET /Software.html HTTP/1.0" 200 1497
 *
 * @return: {dict(host, datetime, method, host, protocol, protocol_version, status, document_size)}
 */
exports.tokenizeLine = line => {
    let lp = line.trim().split(' ');
    // 141.243.1.172
    const host = lp.shift();
    // [29:23:53:25]
    const datetime = lp.shift();
    // 1497
    const document_size = lp.pop();
    // 200
    const response_code = lp.pop();
    // "GET /Software.html HTTP/1.0"
    line = lp.join(' ');
    //get rid of 1st and last '"'
    // GET /Software.html HTTP/1.0
    line = line.substring(1, line.length - 1).trim();
    //assume HTTP/1.0 if not set
    if (!line.toLowerCase().match(/ http\/(1.[0,1]|2|3)$/)) {
        line = line + ' HTTP/1.0';
    }
    lp = line.split(' ');
    // HTTP/1.0
    const protoParts = this.getProtoParts(lp.pop());
    // 1.0
    const protocol_version = protoParts.pop();
    // HTTP
    const protocol = protoParts.pop();
    // /Software.html
    const url = lp.pop();
    let method = 'GET';
    // GET
    if (lp.length) method = this.getMethod(lp.pop());

    return {
        host,
        datetime,
        method,
        url,
        protocol,
        protocol_version,
        response_code,
        document_size,
    };
};

/**
 * Get the method of the http-request.
 *
 * @param {string[]} lp - line-parts
 *
 * @return: {string} - the http-method found, defaults to GET if no method was found.
 */
exports.getMethod = method => {
    for (let allowedMethod of constants.HTTP_METHODS) {
        if (method.toUpperCase() === allowedMethod) return method.toUpperCase();
    }
    return 'GET';
};

/**
 * creates a request-object based on a tokenized line from the file-resource.
 *
 * @param {string[]} tokens -
 *
 * @return {dict} - the request-object
 */
exports.getReqObj = tokens => {
    //ability to take an untokenized line
    if (typeof tokens == 'string') tokens = this.tokenizeLine(tokens);
    if (isNaN(tokens.document_size)) tokens.document_size = '0';
    return this.validateReqObj({
        host: tokens.host,
        datetime: this.getTimeObj(this.getTimeString(tokens.datetime)),
        request: {
            method: tokens.method,
            url: tokens.url,
            protocol: tokens.protocol,
            protocol_version: tokens.protocol_version,
        },
        response_code: tokens.response_code,
        document_size: tokens.document_size,
        valid: false,
    });
};

/**
 * checks integrity of reqObj and sets flag valid accordingly
 *
 * @param {dict} reqObj -
 *
 * @return {dict} -
 */
exports.validateReqObj = reqObj => {
    try {
        decodeURIComponent(reqObj.request.url);
        reqObj.valid = true;
    } catch (e) {
        console.log(reqObj.request.url);
    }
    return reqObj;
};

/**
 * Creates an array of request-objects.
 *
 * @return: {dict[]} -
 */
exports.getReqObjArr = () => {
    requests = [];
    const content = this.readLines('resources/epa-http.txt', 'utf8');
    let id = 0;
    content.map(line => {
        const obj = this.getReqObj(line);
        obj.id = ++id;
        requests.push(obj);
    });
    return requests;
};

/**
 * main() wrapper
 */
const main = () => {
    const requests = this.getReqObjArr();

    this.writeJSON(requests);
};

//execute only if module is directly called from node (node parser.js)
if (require.main === module) {
    main();
}
