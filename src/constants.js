exports.HTTP_METHODS = ['GET', 'POST', 'HEAD', 'PUT', 'DELETE'];

exports.HTTP_CODES = [
    '200',
    '201',
    '302',
    '304',
    '400',
    '403',
    '404',
    '500',
    '501',
];
