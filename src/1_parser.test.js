const fs = require('fs');

const assert = require('chai').assert;

const parser = require('./parser');

describe('parser.js', function() {
    before(function() {
        if (!fs.existsSync('resources/requests.json')) console.log('not there');
        else console.log('there');
    });

    describe('tokenizeLine()', function() {
        const result = parser.tokenizeLine(
            'wpbfl2-45.gate.net [29:23:54:18] "GET /logos/small_gopher.gif HTTP/1.0" 200 935'
        );
        it('should return type {dict}', function() {
            assert.isObject(result, 'we expected {dict}-type here!');
        });

        it('should return 8 keys', function() {
            assert.lengthOf(
                Object.keys(result),
                8,
                'we expected length 8 here!'
            );
        });
    });

    describe('getTimeString()', function() {
        const result = parser.getTimeString('[29:23:54:18]');
        it('should return type {string}', function() {
            assert.isString(result, 'we expected type {string} here!');
        });

        it('should match 29:23:54:18', function() {
            assert.strictEqual(
                result,
                '29:23:54:18',
                "we expected '29:23:54:18' here!"
            );
        });
    });

    describe('getTimeParts()', function() {
        const result = parser.getTimeParts('29:23:54:18');
        it('should return type {array}', function() {
            assert.isArray(result, 'we expected {array}-type here!');
        });

        it('should return array-length 4', function() {
            assert.lengthOf(result, 4, 'we expected length 4 here!');
        });

        it('should return {array} of numeric values', function() {
            result.map(item =>
                assert.isNumber(
                    parseInt(item),
                    'we expected numeric value here!'
                )
            );
        });

        it('should return numeric value between 1 and 31 @index 0', function() {
            assert.isAbove(
                parseInt(result[0]),
                0,
                'we expected more than 0 here!'
            );
            assert.isBelow(
                parseInt(result[0]),
                32,
                'we expected less than 32 here!'
            );
        });

        it('should return numeric value between 0 and 59 @index 1', function() {
            assert.isAbove(
                parseInt(result[1]),
                -1,
                'we expected more than -1 here!'
            );
            assert.isBelow(
                parseInt(result[1]),
                60,
                'we expected less than 60 here!'
            );
        });

        it('should return numeric value between 0 and 59 @index 2', function() {
            assert.isAbove(
                parseInt(result[2]),
                -1,
                'we expected more than -1 here!'
            );
            assert.isBelow(
                parseInt(result[2]),
                60,
                'we expected less than 60 here!'
            );
        });

        it('should return numeric value between 0 and 59 @index 3', function() {
            assert.isAbove(
                parseInt(result[3]),
                -1,
                'we expected more than -1 here!'
            );
            assert.isBelow(
                parseInt(result[3]),
                60,
                'we expected less than 60 here!'
            );
        });
    });
});
