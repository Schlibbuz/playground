const express = require('express');
const path = require('path');

const analyzer = require('./analyzer');

const PORT = process.env.PORT || 5000;

app = express()
    .use(express.static(path.join(__dirname, '../public')))
    .set('views', path.join(__dirname, '../views'))
    .set('view engine', 'ejs');

/**
 * routes
 */
app.get('/', (req, res) => res.render('pages/index'));

app.get('/api/stats', (req, res) => {
    stats = {};
    stats.reqsPerMin = analyzer.genStats('reqsPerMin');
    stats.distHttpMeths = analyzer.genStats('distHttpMeths');
    stats.distResCodes = analyzer.genStats('distResCodes');
    stats.dist200BodySize = analyzer.genStats('dist200BodySize');
    res.send(stats);
});
/**
 * routes end
 */

app.listen(PORT, () => console.log(`Listening on ${PORT}`));
