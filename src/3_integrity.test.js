const assert = require('chai').assert;

const constants = require('./constants');
const parser = require('./parser');

const addIntegrityCheckErrors = (errors, reqErrors) => {
    if (Object.keys(reqErrors).length <= 1) return;
    const id = reqErrors.id;
    delete reqErrors.id;
    errors[id] = reqErrors;
};
describe('integrity-test', function() {
    describe('reqObjArr', function() {
        const reqObjArr = parser.getReqObjArr();

        it("should have length 47'748", function() {
            assert.lengthOf(reqObjArr, 47748, "we expected 47'748 here!");
        });

        it("should have 46'014 GET-Requests", function() {
            assert.lengthOf(
                reqObjArr.filter(
                    req => req.valid && req.request.method === 'GET'
                ),
                46014,
                "we expected 46'014 here!"
            );
        });

        it("should have 1'622 POST-Requests", function() {
            assert.lengthOf(
                reqObjArr.filter(
                    req => req.valid && req.request.method === 'POST'
                ),
                1622,
                "we expected 1'622 here!"
            );
        });

        it('should have 106 HEAD-Requests', function() {
            assert.lengthOf(
                reqObjArr.filter(
                    req => req.valid && req.request.method === 'HEAD'
                ),
                106,
                'we expected 106 here!'
            );
        });

        it('should have 6 illegal requests', function() {
            assert.lengthOf(
                reqObjArr.filter(req => !req.valid),
                6,
                'we expected 6 here!'
            );
        });

        it('should not have illegal values', function() {
            let legalValues = true;
            const errors = {};
            reqObjArr.map(req => {
                const reqErrors = { id: req.id };
                if (typeof req.host !== 'string' || !req.host.length)
                    legalValues = false;

                try {
                    new Date(
                        '1995-05-' +
                            req.datetime.day +
                            'T' +
                            req.datetime.hour +
                            ':' +
                            req.datetime.minute +
                            ':' +
                            req.datetime.second
                    );
                } catch (err) {
                    reqErrors.datetime =
                        JSON.stringify(req.datetime) + ' date is illegal';
                }

                if (!constants.HTTP_METHODS.includes(req.request.method))
                    reqErrors.method =
                        req.request.method + ' method is illegal';

                //TODO: test failing, simplified for now
                if (req.valid) {
                    try {
                        decodeURIComponent(req.request.url);
                    } catch (err) {
                        console.log(JSON.stringify(err));
                        reqErrors.url = req.request.url;
                    }
                    // if (typeof req.request.url !== 'string') reqErrors.url = req.request.url;
                }

                if (!constants.HTTP_CODES.includes(req.response_code)) {
                    reqErrors.response_code =
                        req.response_code + ' code is illegal';
                }

                if (parseInt(req.document_size) != req.document_size) {
                    reqErrors.document_size =
                        req.document_size + ' doc-size is illegal';
                }

                addIntegrityCheckErrors(errors, reqErrors);
            });
            if (Object.keys(errors).length)
                console.log(JSON.stringify(errors, null, 4));
            assert.isTrue(
                Object.keys(errors).length === 0,
                'we expected the values to be legal!'
            );
        });
    });
});
