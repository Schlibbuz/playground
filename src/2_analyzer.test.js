const assert = require('chai').assert;

const analyzer = require('./analyzer');

describe('analyzer.js', function() {
    const result = analyzer.readJSON();
    describe('readJSON()', function() {
        it("should return type 'string'", function() {
            assert.isString(result, "we expected 'string' here!");
        });

        it('should have content', function() {
            assert.isNotEmpty(result, 'we expected content here!');
        });
    });
});
