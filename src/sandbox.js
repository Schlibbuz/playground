const fs = require('fs');

//module-vars
let lineErrors = 0;

const getTimeObj = time => {
    tps = time.substring(1, time.length - 1).split(':');
    return { day: tps[0], hour: tps[1], minute: tps[2], second: tps[3] };
};

const getReqQbj = reqPart => {
    const pPart = reqPart[2].split('/');
    return {
        method: reqPart[0],
        url: reqPart[1],
        proto: pPart[0],
        pv: pPart[1],
    };
};

const addReqElem = (line, id) => {
    o = {};
    const lA = line.split(' ');
    const length = lA.length;
    if (length !== 7) {
        lineErrors++;
        //id == line-number
        console.log(lA.length + ' ' + line + ' ' + id);
    }
    o.id = id;
    o.host = lA[0];
    o.datetime = getTimeObj(lA[1]);
    o.req = getReqQbj([lA[2], lA[3], lA[4]]);

    return o;
};

const cleanLine = line => {
    return line;
};
const content = fs.readFileSync('resources/epa-http.txt', 'utf8');

let lines = content.trim().split('\n');

lines = lines.map(line => cleanLine());

let id = 1;
oA = lines.map(line => addReqElem(line, id++));
console.log(lineErrors);

const x = oA.filter(o => Object.keys(o.datetime).length > 0);
console.log(lines.length);
console.log(oA.length);
console.log(x.length);
