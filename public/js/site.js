const genLineChart = data => {};

const genPieChart = data => {};

const genChart = (id, type, label, data, backgroundColor) => {
    if (backgroundColor === undefined) {
        if (type === 'line') {
            genLineChart();
            backgroundColor = 'rgb(255, 99, 132)';
        } else {
            genPieChart();
            backgroundColor = chartColors;
        }
    }
    const labels = Object.keys(data);
    const values = Object.values(data);
    const ctx = document.getElementById(id).getContext('2d');
    new Chart(ctx, {
        type: type,

        data: {
            labels: labels,
            datasets: [
                {
                    label: label,
                    backgroundColor: backgroundColor,
                    borderColor: 'rgb(255, 99, 132)',
                    data: values,
                },
            ],
        },

        options: {},
    });
};

$(document).ready(() => {
    $.get('/api/stats', data => {
        console.log(data);
        genChart(
            'distHttpMeths',
            'pie',
            'Distribution of HTTP-Methods',
            data.distHttpMeths
        );
        genChart('reqsPerMin', 'line', 'Requests per minute', data.reqsPerMin);
        genChart(
            'distResCodes',
            'pie',
            'Distribution of Status-Codes',
            data.distResCodes
        );
        genChart(
            'dist200BodySize',
            'pie',
            'Distribution of Body-Sizes',
            data.dist200BodySize
        );
    });
});
